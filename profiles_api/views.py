from rest_framework.views import APIView
from rest_framework.response import  Response

from rest_framework import status
from profiles_api import serializers
#from profiles_api import serializers

class HelloApiView(APIView):
    serializer_class = serializers.HelloSerializer


    def get(self,req,format=None):
        #serializer_class=serializers.HelloSerializers;

        an_api= [
            'Uses HTTP ',
            'Is similar to trad Django View',
            'gives',
            'map'
            ]
        return Response({'message':'hola','an_api':an_api})
    def post(self, request):
        """Create a hello message with our name"""
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            name = serializer.validated_data.get('name')
            message = name
            return Response({'message': message})
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )
    def put(self, request, pk=None):
        """Handle updating an object"""
        return Response({'method': 'PUT'})

    def patch(self, request, pk=None):
        """Handle partial update of object"""
        return Response({'method': 'PATCH'})

    def delete(self, request, pk=None):
        """Delete an object"""
        return Response({'method': 'DELETE'})
# Create your views here.
